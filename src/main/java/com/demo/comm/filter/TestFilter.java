package com.demo.comm.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
 
/**
 * 
 * @author wangfeng
 *
 */
//实现Filter接口，基于回调的方式，类似ajax请求的success。
public class TestFilter implements Filter {
 
    //init方法，初始化过滤器，可以在init()方法中完成与构造方法类似的初始化功能，
    //如果初始化代码中要使用到FillerConfig对象，那么这些初始化代码就只能在Filler的init()方法中编写而不能在构造方法中编写
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
        System.out.println("第一个过滤器成功初始化。。。。。。。。。。。。。");
    }
 
    //doFilter()方法有多个参数，其中
    //参数request和response为Web服务器或Filter链中的上一个Filter传递过来的请求和响应对象；
    //参数chain代表当前Filter链的对象，
    //只有在当前Filter对象中的doFilter()方法内部需要调用FilterChain对象的doFilter()法才能把请求交付给Filter链中的下一个Filter或者目标程序处理
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest req = (HttpServletRequest) servletRequest;
//        //这里为了使用getHeader方法获取token，转型成HttpServletRequest
//        System.out.println("token:"+req.getHeader("token"));
//        String token = req.getHeader("token");
//        //再判断token是否正确
//        if(null==token){
//            throw new RuntimeException("token为空");
//        }
        //调用doFilter方法，正常返回servletResponse
    	System.out.println("这里是TestFilter的拦截器");
        filterChain.doFilter(servletRequest, servletResponse);
    }
 
    //destroy()方法在Web服务器卸载Filter对象之前被调用，该方法用于释放被Filter对象打开的资源，例如关闭数据库和I/O流
    @Override
    public void destroy() {
        Filter.super.destroy();
        System.out.println("过滤器被销毁");
    }
}