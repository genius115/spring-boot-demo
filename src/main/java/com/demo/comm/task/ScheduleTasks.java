package com.demo.comm.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ScheduleTasks {
	@Autowired
	private AsyncTasks asyncTasks;
	
	@Scheduled(cron = "0 10 * * * ?")
	public void task1() {
		log.info(Thread.currentThread().getName() +"/ScheduleTasks task1...");
		asyncTasks.task1();
	}

	@Scheduled(cron = "0 30 * * * ?")
	public void task2() throws Exception {
		System.out.println(Thread.currentThread().getName() + "开始");
		Thread.sleep(5000);
		System.out.println(Thread.currentThread().getName() + "结束");
	}
}
