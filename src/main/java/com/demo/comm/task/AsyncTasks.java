package com.demo.comm.task;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AsyncTasks {
	private static ThreadPoolExecutor PRINT_THREAD_POOL = new ThreadPoolExecutor(5, 5, 500, TimeUnit.SECONDS,
			new LinkedBlockingQueue<>(100), new ThreadFactoryBuilder().setNameFormat("PRINT_POOL- %d").build(),
			new ThreadPoolExecutor.AbortPolicy());
//	ThreadPoolExecutor executor = new ThreadPoolExecutor( // 1.创建线程池对象
//            1, // 核心线程数
//            2, // 最大线程数量，即核心和非核心线程数量之和
//            500, // 非核心线程的最大空闲时间，查过这个时间，相应非核心线程就会被回收
//            TimeUnit.SECONDS, // 上一个参数的时间单位
//            new LinkedBlockingDeque<>(), // 阻塞队列设置
//            new ThreadFactory() { // 生产线程的工厂，实现接口ThreadFactory
//                @Override // 重写创建新线程的方法
//                public Thread newThread(Runnable r) {
//                    Thread t = new Thread(r);
//                    t.setName("测试-"); // 可以自定义设置新线程的名字
//                    return t;
//                }
//            },
//            new ThreadPoolExecutor.AbortPolicy() // 设置拒绝策略
//    );
	
	public static String printID(String id) {
		log.info(Thread.currentThread().getName() +"/printID");
		return "printID:"+id;		
	}
	@Async
	public void task1() {
		log.info(Thread.currentThread().getName() +"/AsyncTasks task1...");
	}
	@Async
	public void task2() {
		PRINT_THREAD_POOL.execute(new Runnable() { // 2.调用execute或submit方法执行Runnable任务或Callable任务
			@Override
			public void run() {
				log.info(Thread.currentThread().getName() +"/execute");
				System.out.println("love love love");
			}
		});
	}
	@Async
	public void task3() {
		Future<Object> future = PRINT_THREAD_POOL.submit(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				log.info(Thread.currentThread().getName() +"/execute");
				return null;
			}
		});
		try {
			future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
	@Async
	public CompletableFuture<String> task4(String id) {
		CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(()->printID(id),PRINT_THREAD_POOL);
		return completableFuture;
	}

}
