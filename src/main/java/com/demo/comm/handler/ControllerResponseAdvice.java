package com.demo.comm.handler;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.demo.comm.bean.ResultCode;
import com.demo.comm.bean.ResultVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 统一响应
 * 
 *  对返回数据增强，封装成统一格式
 * 1、ResponseBodyAdvice 需要绑定到 {@link @RestControllerAdvice} 或者 {@link @ControllerAdvice} 才能生效。
 * 2、注意仅对返回值为 ResponseEntity 或者是有 @ResponseBody 注解的控制器方法进行拦截，
 * * @RestController 标记的类，相当于是类中的所有方法上都加了 @ResponseBody。
 * 3、@RestControllerAdvice 默认是针对所有的控制器，但也可以指定某个包，及其子包都会进行拦截。
 * 
 * ResponseBodyAdvice 接口允许在执行 @ResponseBody 或 @ResponseEntity 控制器方法之后，
 * 但在使用 HttpMessageConverter 写入响应体之前自定义响应，进行功能增强。通常用于 加密，签名，统一数据格式等。
 * @author wangfeng
 *
 */
@Slf4j
@RestControllerAdvice(basePackages = {"com.demo"})
public class ControllerResponseAdvice implements ResponseBodyAdvice<Object> {
   
	/**
	 * 1、选择是否执行 beforeBodyWrite 方法，返回 true 执行，false 不执行.
     * 2、通过 supports 方法，可以选择对哪些类或方法的 Response 进行处理，其余的则不处理。
     * @param returnType：返回类型
     * @param converterType：转换器
     * @return ：返回 true 则下面的 beforeBodyWrite  执行，否则不执行
	 */
	@Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
		// response是ResultVo类型，response是ResponseEntity类型, 或者注释了NotControllerResponseAdvice都不进行包装
        return !(methodParameter.getParameterType().isAssignableFrom(ResultVo.class)
                || methodParameter.hasMethodAnnotation(NotControllerResponseAdvice.class)
                || methodParameter.getParameterType().isAssignableFrom(ResponseEntity.class));
    }

	/**
	 * 
    * 对 Response 处理的具体执行方法
    * 
    * @param body：响应对象(response)中的响应体
    * @param returnType：控制器方法的返回类型
    * @param selectedContentType：通过内容协商选择的内容类型
    * @param selectedConverterType：选择写入响应的转换器类型
    * @param request：当前请求
    * @param response：当前响应
    * @return ：返回传入的主体或修改过的(可能是新的)主体
	 */
    @Override
    public Object beforeBodyWrite(Object data, MethodParameter returnType, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest request, ServerHttpResponse response) {
    	// String类型不能直接包装
        if (returnType.getGenericParameterType().equals(String.class)) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                // 将数据包装在ResultVo里后转换为json串进行返回
                return objectMapper.writeValueAsString(new ResultVo(data));
            } catch (JsonProcessingException e) {
                throw new APIException(ResultCode.RESPONSE_PACK_ERROR, e.getMessage());
            }
        }
        // 否则直接包装成ResultVo返回
        return new ResultVo(data);
    }
}
