package com.demo.comm.handler;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.demo.comm.interceptor.LoginIntercept;

/**
 * 映射非默认路径为资源路径
 * 注意：必须以“/”结尾，访问的路径
 * 示例：
 * 1、磁盘D:/test/file/1.jpg  浏览器http://localhost:10086/picture/1.jpg
 * 
 * @author wangfeng
 *
 */
@Component
public class MyResHandler extends WebMvcConfigurerAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//或者registry.addResourceHandler("/res/**").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
		//定义到新文件夹
        registry.addResourceHandler("/image/**").addResourceLocations("classpath:/image/");
        //定义到硬盘
        registry.addResourceHandler("/picture/**").addResourceLocations("file:D:/test/file/");
		super.addResourceHandlers(registry);
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
//		registry.addInterceptor(new LoginIntercept()).addPathPatterns("/**").excludePathPatterns("/login");
//		super.addInterceptors(registry);
	}
}
