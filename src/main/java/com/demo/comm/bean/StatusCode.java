package com.demo.comm.bean;

/**
 * ​状态码​​​的接口，所有​​状态码​​都需要实现它
 * @author wangfeng
 *
 */
public interface StatusCode {
    public int getCode();
    public String getMsg();
}