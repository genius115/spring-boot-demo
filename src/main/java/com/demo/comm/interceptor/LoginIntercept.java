package com.demo.comm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

/**
 * 如果你需要自定义 Interceptor 的话必须实现 org.springframework.web.servlet.HandlerInterceptor接口或继承 org.springframework.web.servlet.handler.HandlerInterceptorAdapter类，并且需要重写下面下面 3 个方法：
 * @author wangfeng
 *
 *preHandle：拦截于请求刚进入时，进行判断，需要 boolean 返回值，如果返回 true 将继续执行，如果返回 false，将不进行执行。一般用于登录校验。
 *postHandle：拦截于方法成功返回后，视图渲染前，可以对 modelAndView 进行操作。
 *afterCompletion：拦截于方法成功返回后，视图渲染前，可以进行成功返回的日志记录。
 *
 *https://www.yisu.com/zixun/697322.html
 *
 *https://blog.csdn.net/baidu_38798835/article/details/120416168
 *
 *https://blog.csdn.net/ylx1066863710/article/details/124788843
 *
 */
@Slf4j
@Component
public class LoginIntercept implements HandlerInterceptor{
	
	/**
	 * 预处理，在业务处理器处理请求之前被调用，可以进行登录拦截，编码处理、安全控制、权限校验等处理；
	 */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	log.info("********************LoginIntercept-preHandle********************");
    	return true;
    }
 
    /**
     * 后处理，在业务处理器处理请求执行完成后，生成视图之前被调用。即调用了Service并返回ModelAndView，但未进行页面渲染，可以修改ModelAndView，这个比较少用。
     */
    @Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
    	log.info("********************LoginIntercept-postHandle********************");
	}
    
    /**
     * 后处理，在业务处理器处理请求执行完成后，生成视图之前被调用。即调用了Service并返回ModelAndView，但未进行页面渲染，可以修改ModelAndView，这个比较少用。
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        UserContext.clean();
    	//清空线程池中信息，放置内存泄露
    	log.info("********************LoginIntercept-afterCompletion********************");    	
    }
}
