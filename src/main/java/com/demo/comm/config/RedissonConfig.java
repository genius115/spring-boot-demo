package com.demo.comm.config;

import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

	@Value("${spring.redis.host}")
	private String redisHost;
	@Value("${spring.redis.port}")
	private String redisPort;
	@Value("${spring.redis.password}")
	private String redisPassword;

	@Bean
	RedissonClient redissonClient() {
		// 默认连接地址 127.0.0.1:6379
//		RedissonClient redisson = Redisson.create();
		Config config = new Config();
		config.useSingleServer().setAddress("redis://" + redisHost + ":" + redisPort) // redis服务器地址 redisAddress
				.setDatabase(0) // 制定redis数据库编号
				.setPassword(StringUtils.isNoneBlank(redisPassword) ? redisPassword : null) // redis用户名密码
				.setConnectionMinimumIdleSize(10) // 连接池最小空闲连接数
				.setConnectionPoolSize(50) // 连接池最大线程数
				.setIdleConnectionTimeout(60000) // 线程的超时时间
				.setConnectTimeout(6000) // 客户端程序获取redis链接的超时时间
				.setTimeout(60000); // 响应超时时间
		RedissonClient redisson = Redisson.create(config);
		return redisson;
	}
}
