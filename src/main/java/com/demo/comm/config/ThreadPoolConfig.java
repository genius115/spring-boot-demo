package com.demo.comm.config;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class ThreadPoolConfig implements AsyncConfigurer {

//	@Override
//	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//		taskRegistrar.setScheduler(Executors.newScheduledThreadPool(2));
//	}

	@Bean
	Executor TaskScheduler() {
		ThreadPoolTaskScheduler pool = new ThreadPoolTaskScheduler();
		pool.setPoolSize(2);
		pool.setThreadNamePrefix("TASK_Scheduler-");
		return pool;
	}
	
	@Bean
	Executor taskExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(5);
		pool.setMaxPoolSize(5);
		pool.setKeepAliveSeconds(60);
		pool.setQueueCapacity(100);
		pool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		pool.setThreadNamePrefix("Slave_Executor-");
		pool.initialize();
		return pool;
	}

	@Primary
	@Bean
	ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(5);
		pool.setMaxPoolSize(5);
		pool.setKeepAliveSeconds(61);
		pool.setQueueCapacity(100);
		pool.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		pool.setThreadNamePrefix("Pri_Executor-");
		pool.initialize();
		return pool;
//        return Executors.newSingleThreadExecutor();
		// return Executors.newScheduledThreadPool(10);
	}

	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(5);
		pool.setMaxPoolSize(10);
		pool.setKeepAliveSeconds(60);
		pool.setQueueCapacity(100);
		pool.setThreadNamePrefix("Async-");
		pool.initialize();
		return pool;
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		// return AsyncConfigurer.super.getAsyncUncaughtExceptionHandler();
		return (Throwable ex, Method method, Object... params) -> {
			String errorBuilder = "Async execution error on method:" + method.toString() + " with parameters:"
					+ Arrays.toString(params);
			log.error(errorBuilder);
		};
	}

}
