package com.demo.forest;

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.http.ForestRequest;

public interface MyForestClient {
	 
	/**
     * Get类型请求，url路径为 /test
     * <p>ForestRequest是带泛型参数的类型
     * <p>泛型参数代表返回的响应数据所期望转化成的类型
     * 
     * @return Forest请求对象
     */
    @Get("http://82.157.160.26:18081/test/helloword")
    ForestRequest<?> getForestRequest();

}
