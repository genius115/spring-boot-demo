package com.demo.forest;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dtflys.forest.Forest;
import com.dtflys.forest.http.ForestRequest;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/forest")
public class ForestController {
	private final static Logger logger = LoggerFactory.getLogger(ForestController.class);
	
	@Resource
	MyForestClient myForestClient;

	// 如果请求方法以 ForestRequest 作为返回值类型
	// 不会直接发送请求
	// 而是直接返回 Forest 请求对象
	@GetMapping("/1")
	public String test1() {
		ForestRequest<?> request = myForestClient.getForestRequest();
		// 得到字符串 /test
		String path = request.path();
		logger.info("testLog 接口");
		log.info("123456789"); //用法添加-javaagent:lombok.jar 安装lombok.jar
		// 手动执行发送请求，并以字符串形式接受响应数据
		String ret = request.execute(String.class);
		return ret;
	}
	@GetMapping("/2")
	public String test2() {
		// Get请求
		// 并以 String 类型接受数据
		String str = Forest.get("http://82.157.160.26:18081/test/helloword").executeAsString();
		return str;	
	}
	
	

}
