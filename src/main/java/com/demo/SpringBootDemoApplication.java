package com.demo;

import java.util.Date;
import java.util.TimeZone;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.comm.bean.JSONResult;
import com.demo.comm.bean.sys.SystemServer;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.ApiOperation;

@EnableScheduling // 定时任务
@EnableAsync // 异步任务
@RestController
@SpringBootApplication
//@MapperScan ( "com.demo.samples.mapper" ) //或者卸载Mapper文件上
//@MapperScan ({ "com.demo.samples.mapper" , "com.demo.test.mapper" }) 
@MapperScan("com.demo.**.mapper") // 一个 * 代表一级包 ,两个 * 代表任意个包
public class SpringBootDemoApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
		SpringApplication.run(SpringBootDemoApplication.class, args);
		System.out.println("-------------------------------------------------------------------\n"
				+ "            启动成功！！！欢迎使用！！！永无Bug！！！   ლ(´ڡ`ლ)ﾞ                     \n"
				+ "            启动成功！！！欢迎使用！！！永无Bug！！！   ლ(´ڡ`ლ)ﾞ                     \n"
				+ "            启动成功！！！欢迎使用！！！永无Bug！！！   ლ(´ڡ`ლ)ﾞ                     \n"
				+ "-------------------------------------------------------------------\n" + "当前时间：" + DateUtil.now());
	}

	@Value("${spring.application.name:applicationname}")
	private String applicationname;

	@Value("${server.port:8080}")
	private String port;

	@GetMapping({ "/", "" })
	public String serverInfo() {
		System.out.println(System.getProperty("user.dir"));
		String r = "时间:[" + new Date() + "]---应用名称:[" + applicationname + "]---端口号:[" + port + "]---分支: [master]";
		return r;
	}

	@GetMapping("/index")
	public String indexPage() {
		String r = "欢迎您！打开docker/jenkins/springboot之旅。";
		return r;
	}

	@GetMapping("/server")
	@ApiOperation("查询服务器信息")
	public JSONResult server() {
		SystemServer server = new SystemServer();
		try {
			server.copyTo();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new JSONResult(200, "Get CPU Mem FileSystem!!!", server);
	}
}
