package com.demo.samples.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.samples.dao.SimpleUser;
import com.demo.samples.dao.User;
import com.demo.samples.mapper.UserMapper;
import com.demo.samples.service.UserService;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="用户模块")
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
    private UserService userService;
	
	@Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;
	
	@Autowired
    private UserMapper userMapper;
	
	@ApiOperation(value = "分页查询")
    @GetMapping("/pageUser")
	public PageInfo<User> pageUser(HttpServletRequest request){
		return userService.pageUser(request);
	}
	
	@ApiOperation(value = "访问次数")
    @GetMapping("/online")
	public String onLine() {
		 Long count = stringRedisTemplate.opsForValue().increment("views",1L);
	     return "Hello, Thank you ["+ count +"] view!!!";
	}
	
	@ApiOperation(value = "获取所有用户")
    @GetMapping("/getList")
	public List<User> getList() {
        System.out.println(("----- getList method test ------"));
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);        
        return userList;
    }
    
	@ApiOperation(value = "获取用户依据Id")
    @GetMapping("/getById")
   	public List<User> getById(@RequestParam("id") String id) {
    	System.out.println(("----- getById method test ------"));
         List<User> userList = userMapper.getUserById(id);
         userList.forEach(System.out::println);
         return userList;
    }
	
	/**
	 * 添加用户1
	 * @param user
	 * @return
	 */
	@PostMapping(value = "/add1")
	@ResponseBody
	public List<SimpleUser> add1(@Validated SimpleUser user) {
		List<SimpleUser> list = new ArrayList<SimpleUser>();
		list.add(new SimpleUser("zhangsan","123456","zs","18612345678"));
		return list;
	}
	/**
	 * 添加用户11
	 * @param user
	 * @return
	 */
	@PostMapping(value = "/add11")
	@ResponseBody
	public List<SimpleUser> add11(@ModelAttribute SimpleUser user) {
		List<SimpleUser> list = new ArrayList<SimpleUser>();
		list.add(new SimpleUser("zhangsan11","123456","zs","18612345678"));
		return list;
	}
	/**
	 * 添加用户2
	 * @param user
	 * @return
	 */
	@PostMapping(value = "/add2")
	@ResponseBody
	public List<SimpleUser> add2(@RequestBody  SimpleUser user) {
		List<SimpleUser> list = new ArrayList<SimpleUser>();
		list.add(new SimpleUser("zhangsan2","123456","zs","18612345678"));
		return list;
	}
	/**
	 * 添加用户2
	 * @param user
	 * @return
	 */
	@GetMapping(value = "/del")
	@ResponseBody
	public String del(@ModelAttribute SimpleUser user) {
		System.out.println(user.getUsername());
		return "OK";
	}

}
