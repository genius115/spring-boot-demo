package com.demo.samples.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.samples.dao.User;
import com.demo.samples.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class UserService {
	
	@Autowired
    private UserMapper userMapper;
	
	public PageInfo<User> pageUser(HttpServletRequest request){
		//request: url?pageNum=1&pageSize=10
		//支持 ServletRequest,Map,POJO 对象，需要配合 params 参数
		PageHelper.startPage(request);
		List<User> list = userMapper.selectAll();
		//用PageInfo对结果进行包装
		PageInfo<User> page = new PageInfo<User>(list);
		return page;
	}

}
