package com.demo.samples.dao;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;


/**
 * 只要你写在字段上加了JSR303的验证注解或者是hibernate-validate的注解。smart-doc在输出请求参数文档时自动填写参数必填为true
 * @author wangfeng
 *
 */
public class SimpleUser {
	/**
	 * 用户名(记录字段变更)
	 * @since 1.0
	 */
	@NotNull
	private String username;
	/**
	 * 密码
	 */
	@NotEmpty
	@Length(max = 5)
	private String password;
	/**
	 * 昵称（请求忽略字段）
	 * @ignore
	 */	
	private String nickname;
	/**
	 * 电话
	 */
	@NotBlank(message = "电话号码不能为空")
    @Pattern(regexp = "^1[3-8]{1}[0-9]{9}$", message = "电话号码不正确")
	private String mobile;

	public SimpleUser() {
	}

	public SimpleUser(@NotNull String username, String password, String nickname, String mobile) {
		this.username = username;
		this.password = password;
		this.nickname = nickname;
		this.mobile = mobile;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
