package com.demo.samples.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.samples.dao.User;

@Mapper
public interface UserMapper extends BaseMapper<User> {

	List<User> getUserById (@Param("id")String id);
	
	List<User> selectAll();
}
