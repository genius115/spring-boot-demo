package com.demo.test.service;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class StockRedissonService {

	@Autowired
	private StringRedisTemplate redisTemplate;

	@Autowired
	private RedissonClient redissonClient;

	public void deduct() {
		RLock rlock = redissonClient.getLock("lock");
		rlock.lock();
//		rlock.tryLock(1000, TimeUnit.MICROSECONDS);
//		rlock.tryLock(1000, 5000, TimeUnit.MICROSECONDS);
		Boolean flag = rlock.tryLock();
		if (!flag) {
			return;
		}
		try {
			// 1。 查询库存
			String stockStr = redisTemplate.opsForValue().get("stock");
			if (!StringUtils.isBlank(stockStr)) {
				int stock = Integer.parseInt(stockStr);
				// 2。 判断条件是否满足
				if (stock > 0) {
					// 3 更新redis
					redisTemplate.opsForValue().set("stock", String.valueOf(stock - 1));
				}
			}
		} finally {
			rlock.unlock();
		}
	}

	public void fairLock(Long id) {
		RLock fairLock = redissonClient.getFairLock("fairLock");
		fairLock.lock();
		try {
			TimeUnit.SECONDS.sleep(3);
			System.out.println("-----测试公平锁-----");
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			fairLock.unlock();
		}
	}

	public void readLock() {
		RReadWriteLock lock = redissonClient.getReadWriteLock("rwLock");
		lock.readLock().lock(10, TimeUnit.SECONDS);
		// TODO 疯狂的读
//        lock.readLock().unlock();
	}

	public void writeLock() {
		RReadWriteLock lock = redissonClient.getReadWriteLock("rwLock");
		lock.writeLock().lock(10, TimeUnit.SECONDS);
		// TODO 疯狂的写
//        lock.writeLock().unlock();
	}

}
