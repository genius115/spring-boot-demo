package com.demo.test.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.samples.dao.User;
import com.ejlchina.searcher.BeanSearcher;
import com.ejlchina.searcher.MapSearcher;
import com.ejlchina.searcher.SearchResult;
import com.ejlchina.searcher.util.MapUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;


@Api(tags="项目测试模块")
@RestController
@RequestMapping("/test")
public class TestController {	
	
	/**
	 * 注入 Map 检索器，它检索出来的数据以 Map 对象呈现
	 */
	@Autowired
	private MapSearcher mapSearcher;

	/**
	 * 注入 Bean 检索器，它检索出来的数据以 泛型 对象呈现
	 */
	@Autowired
	private BeanSearcher beanSearcher;
	

	@GetMapping("/index")
    public SearchResult<Map<String, Object>> index(HttpServletRequest request) {
        // 一行代码，实现一个用户检索接口（MapUtils.flat 只是收集前端的请求参数）
        return mapSearcher.search(User.class, MapUtils.flat(request.getParameterMap()));
    }
	
	
	@ApiOperation("helloword")
	@GetMapping("/helloword")
	public String HellWord(){
		return "Hello World!!!时间:["+new Date()+"]";
	}
	
    @ApiImplicitParam(name = "name",value = "姓名",required = true)
    @ApiOperation(value = "向客人问好")
    @GetMapping("/sayHi")
    public ResponseEntity<String> sayHi(HttpServletRequest req
    		,HttpServletResponse res
    		,@RequestParam(value = "name")String name){ 	
        return ResponseEntity.ok("Hi:"+name);
    }
}
