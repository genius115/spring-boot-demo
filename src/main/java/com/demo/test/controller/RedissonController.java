package com.demo.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.test.service.StockRedissonService;

@RestController
@RequestMapping("/redisson")
public class RedissonController {

	@Autowired
	private StockRedissonService stockService;

	@GetMapping("/deduct}")
	public String deduct(@PathVariable("id") Long id) {
		stockService.deduct();
		return "deduct success!";
	}

	@GetMapping("/fair/lock/{id}")
	public String fairLock(@PathVariable("id") Long id) {
		stockService.fairLock(id);
		return "hello fair lock";
	}

	@GetMapping("/read/lock")
	public String readLock() {
		stockService.readLock();
		return "read read lock";
	}

	@GetMapping("/write/lock")
	public String writeLock() {
		stockService.writeLock();
		return "write write lock";
	}

}
