package com.demo.test.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.demo.test.pojo.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("hello")
public class HelloController {

	@GetMapping("getUser")
	@ResponseBody
	public User getUserMethod() {
		User u = new User();
		u.setName("genius");
		u.setAge(18);
		u.setBitrhday(new Date());
		u.setPassword("pwd");
		u.setDesc("genius~~~");
		log.info(u.toString());
		return u;
	}

	// 1.1转发
	@GetMapping("index1")
	@ResponseBody
	public ModelAndView index1() {
		ModelAndView modelAndView = new ModelAndView("/jsp/index.html");
		return modelAndView;
	}

	// 1.2转发
	@GetMapping("index3")
	public void index3(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/hello.png").forward(request, response);
	}

	// 2.1重定向
	@GetMapping("index2")
	@ResponseBody
	public void index2(HttpServletResponse response) throws IOException {
		response.sendRedirect("/hello.png");
	}

	@GetMapping("/saveUser/{id}")
	@ResponseBody
	public User saveUser(@PathVariable String id, @ModelAttribute User user) {
		System.out.println("请求参数：" + id);
		System.out.println("请求实体：" + user.getName());
		User u = new User();
		u.setName("genius");
		u.setAge(18);
		u.setBitrhday(new Date());
		u.setPassword("pwd");
		u.setDesc("genius~~~");
		log.info(u.toString());
		return u;
	}
}
