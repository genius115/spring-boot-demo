package com.demo.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.samples.dao.User;
import com.demo.test.service.TestNgService;

import io.swagger.annotations.Api;

@Api(tags="TestNG")
@RestController
public class TestNgController {

	@Autowired
	private TestNgService testNgService;

	@PostMapping("/v1/user/add")
	public String addUser(String userName, String password) {
		boolean result = testNgService.saveUser(userName, password);
		return result ? "{\"code\":\"200\",\"msg\":\"用户保存成功\"}" : "{\"code\":\"500\",\"msg\":\"用户保存失败\"}";
	}
 
	@GetMapping("/v1/user/get")
	public String getUser(String userName) {
		boolean result = testNgService.getUserByName(userName);
		return result ? "{\"code\":\"200\",\"msg\":\"获取用户成功\"}" : "{\"code\":\"500\",\"msg\":\"获取用户失败\"}";
	}

	@PostMapping("/v1/user/update")
	public String updateUser(@RequestBody User user) {
		boolean result = testNgService.updateUser(user);
		return result ? "{\"code\":\"200\",\"msg\":\"用户修改成功\"}" : "{\"code\":\"500\",\"msg\":\"用户修改失败\"}";
	}
}