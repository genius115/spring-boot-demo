package com.demo.test;

import com.demo.samples.dao.User;
import com.demo.samples.mapper.UserMapper;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

@Slf4j
@SpringBootTest
public class Testng extends AbstractTestNGSpringContextTests {
	@Autowired
	private UserMapper userMapper;

	@BeforeAll
	public static void setup_data() {
		System.out.println("this is beforeall set up");
	}

	@AfterAll
	public static void tear_down_date() {
		System.out.println("this is test end");
	}

	@BeforeEach
	public void set_up() {
		System.out.println("this is before_each set up");
	}

	@AfterEach
	public void tear_down_each() {
		System.out.println("this is a after_each");
	}

	@DisplayName("上下文")
	@Test
	void contextLoads() {
		System.out.println("**********contextLoads*************");
	}

	@DisplayName("测试查询")
	@Test
	void testSelect() {
		System.out.println(("----- selectAll method test ------"));
		List<User> userList = userMapper.selectList(null);
		assertEquals(5, userList.size());
		userList.forEach(System.out::println);
	}
	@Test
	void test1() {
		String a = "A";
		for(int i=0;i<5;i++) {
			a=String.join(",",a, String.valueOf(i));
		}
		log.info(a);
	}
}