package com.demo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


@SpringBootTest
@AutoConfigureMockMvc(print = MockMvcPrint.NONE,printOnlyOnFailure = true)
public class TestNgControllerTest extends AbstractTestNGSpringContextTests {
	@Autowired
	private MockMvc mockMvc;
	
	// 通用返回结果的封装
	public String getResultCode(MockHttpServletRequestBuilder requestBuilder) {

		try {
			MvcResult mvcResult = mockMvc.perform(requestBuilder).andDo(print()).andExpect(status().isOk()).andReturn();
			String result = mvcResult.getResponse().getContentAsString();
			JSONObject jsonObject = JSON.parseObject(result);
			return jsonObject.getString("code");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Test
    public void testAddUser() {
        System.out.println("线程="+Thread.currentThread().getName()+"执行testAddUser");
        //保存成功
        {
            MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/user/add")
                    .param("userName", "张三")
                    .param("password", "123");
            String resultCode = getResultCode(requestBuilder);
            assertEquals("200", resultCode);
        }
        //保存失败的情况
        {
            MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/user/add")
                    .param("userName", "李四")
                    .param("password", "123");
            String resultCode = getResultCode(requestBuilder);
            assertEquals("200", resultCode);
        }
    }
}