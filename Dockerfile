FROM openjdk:8-jdk-alpine
VOLUME /logs
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENV JAVA_OPTS="-Xms128m -Xmx256m"
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]